package com.ty.carznation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarzNationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarzNationApplication.class, args);
	}

}
